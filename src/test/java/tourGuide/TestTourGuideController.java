package tourGuide;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.service.impl.TourGuideServiceImpl;
import tripPricer.Provider;

@SpringBootTest
@AutoConfigureMockMvc
@Disabled
public class TestTourGuideController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private static TourGuideServiceImpl tourGuideServiceImpl;

    private static User testUser;
    private static VisitedLocation testVisitedLocation;
    private static List<Attraction> testClosestFiveAttractions = new ArrayList<>();
    private static List<UserReward> testUserRewards = new ArrayList<>();
    private static List<Provider> testTripDeals = new ArrayList<>();

    @BeforeAll
    public static void setUp() {
        String userName = "jon";
        String phone = "000";
        String email = userName + "@tourGuide.com";
        testUser = new User(UUID.randomUUID(), userName, phone, email);

        testVisitedLocation = new VisitedLocation(UUID.randomUUID(), new Location(0, 0), new Date());

        Attraction testAttraction = new Attraction("attraction name", "000", "attraction@gmail.com", 0, 0);
        for (int i = 0; i < 5; i++) {
            testClosestFiveAttractions.add(testAttraction);
        }

        UserReward testuReward = new UserReward(testVisitedLocation, testAttraction);
        testUserRewards.add(testuReward);

        Provider testProvider = new Provider(UUID.randomUUID(), "provider name", 0);
        testTripDeals.add(testProvider);

    }

    @BeforeEach
    public void beforeEach() {
        Mockito.when(tourGuideServiceImpl.getUser(testUser.getUserName())).thenReturn(testUser);

        Mockito.when(tourGuideServiceImpl.getUserLocation(testUser)).thenReturn(testVisitedLocation);

        Mockito.when(tourGuideServiceImpl.getNearByAttractions(testVisitedLocation))
                .thenReturn(testClosestFiveAttractions);

        Mockito.when(tourGuideServiceImpl.getUserRewards(testUser)).thenReturn(testUserRewards);

        Mockito.when(tourGuideServiceImpl.getTripDeals(testUser)).thenReturn(testTripDeals);
    }

    @Test
    public void getLocation() throws Exception {
        mockMvc.perform(get("/getLocation").param("userName",
                testUser.getUserName())).andExpect(status().isOk());
    }

    @Test
    public void getNerbyAttractions() throws Exception {
        mockMvc.perform(get("/getNearbyAttractions").param("userName", testUser.getUserName()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    public void getRewards() throws Exception {
        mockMvc.perform(get("/getRewards").param("userName", testUser.getUserName())).andExpect(status().isOk());
    }

    @Test
    public void getAllCurrentLocations() throws Exception {
        mockMvc.perform(get("/getAllCurrentLocations")).andExpect(status().isOk());
    }

    @Test
    public void getTripDeals() throws Exception {
        mockMvc.perform(get("/getTripDeals").param("userName", testUser.getUserName())).andExpect(status().isOk());
    }
}
