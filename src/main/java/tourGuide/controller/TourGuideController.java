package tourGuide.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.model.NearAttraction;
import tourGuide.model.user.User;
import tourGuide.service.impl.RewardsServiceImpl;
import tourGuide.service.impl.TourGuideServiceImpl;
import tripPricer.Provider;

@RestController
public class TourGuideController {

    @Autowired
    TourGuideServiceImpl tourGuideService;

    @Autowired
    RewardsServiceImpl rewardsService;

    @Autowired
    RewardCentral rewardCentral;

    /**
     * @return String
     */
    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * @param userName
     * @return String
     */
    @GetMapping("/getLocation")
    public String getLocation(@RequestParam String userName) {
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        return JsonStream.serialize(visitedLocation.location);
    }

    /**
     * @param userName
     * @return String
     */
    // Instead: Get the closest five tourist attractions to the user - no matter how
    // far away they are.
    // Return a new JSON object that contains:
    // Name of Tourist attraction,
    // Tourist attractions lat/long,
    // The user's location lat/long,
    // The distance in miles between the user's location and each of the
    // attractions.
    // The reward points for visiting each Attraction.
    // Note: Attraction reward points can be gathered from RewardsCentral
    @GetMapping("/getNearbyAttractions")
    public String getNearbyAttractions(@RequestParam String userName) {
        User user = tourGuideService.getUser(userName);
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
        List<Attraction> closestFiveAttractions = tourGuideService.getNearByAttractions(visitedLocation);
        List<NearAttraction> res = new ArrayList<>();
        for (Attraction attraction : closestFiveAttractions) {
            final NearAttraction nearAttraction = new NearAttraction(attraction.attractionName, attraction,
                    visitedLocation.location, rewardsService.getDistance(attraction, visitedLocation.location),
                    rewardCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId()));
            res.add(nearAttraction);
        }
        return JsonStream.serialize(res);
    }

    /**
     * @param userName
     * @return String
     */
    @GetMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
        return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }

    /**
     * @return String
     */
    @GetMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {
        // - Note: does not use gpsUtil to query for their current location,
        // but rather gathers the user's current location from their stored location
        // history.
        //
        // Return object should be the just a JSON mapping of userId to Locations
        // similar to:
        // {
        // "019b04a9-067a-4c76-8817-ee75088c3822":
        // {"longitude":-48.188821,"latitude":74.84371}
        // ...
        // }
        List<User> users = tourGuideService.getAllUsers();
        Map<String, Location> locationsByUserId = new HashMap<>();
        for (User user : users) {
            locationsByUserId.put(user.getUserId().toString(), tourGuideService.getUserLocation(user).location);
        }

        return JsonStream.serialize(locationsByUserId);
    }

    /**
     * @param userName
     * @return String
     */
    @GetMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
        List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
        return JsonStream.serialize(providers);
    }

    /**
     * @param userName
     * @return User
     */
    private User getUser(String userName) {
        return tourGuideService.getUser(userName);
    }

}