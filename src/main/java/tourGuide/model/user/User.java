package tourGuide.model.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import gpsUtil.location.VisitedLocation;
import tripPricer.Provider;

public class User {
	private final UUID userId;
	private final String userName;
	private String phoneNumber;
	private String emailAddress;
	private Date latestLocationTimestamp;
	private List<VisitedLocation> visitedLocations = new ArrayList<>();
	private List<UserReward> userRewards = new ArrayList<>();
	private UserPreferences userPreferences = new UserPreferences();
	private List<Provider> tripDeals = new ArrayList<>();
	private ReentrantLock lockVisitedLocation = new ReentrantLock();
	private ReentrantLock lockUserRewards = new ReentrantLock();

	public User(UUID userId, String userName, String phoneNumber, String emailAddress) {
		this.userId = userId;
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
	}

	/**
	 * @return UUID
	 */
	public UUID getUserId() {
		return userId;
	}

	/**
	 * @return String
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return String
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param emailAddress
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return String
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param latestLocationTimestamp
	 */
	public void setLatestLocationTimestamp(Date latestLocationTimestamp) {
		this.latestLocationTimestamp = latestLocationTimestamp;
	}

	/**
	 * @return Date
	 */
	public Date getLatestLocationTimestamp() {
		return latestLocationTimestamp;
	}

	/**
	 * @param visitedLocation
	 */
	public void addToVisitedLocations(VisitedLocation visitedLocation) {
		lockVisitedLocation.lock();
		try {
			visitedLocations.add(visitedLocation);
		} finally {
			lockVisitedLocation.unlock();
		}
	}

	/**
	 * @return List<VisitedLocation>
	 */
	public List<VisitedLocation> getVisitedLocations() {
		lockVisitedLocation.lock();
		try {
			return visitedLocations;
		} finally {
			lockVisitedLocation.unlock();
		}
	}

	public void clearVisitedLocations() {
		lockVisitedLocation.lock();
		try {
			visitedLocations.clear();
		} finally {
			lockVisitedLocation.unlock();
		}
	}

	/**
	 * @param userReward
	 */
	public void addUserReward(UserReward userReward) {
		lockUserRewards.lock();
		try {
			if (userRewards.stream()
					.filter(r -> r.attraction.attractionName.equals(userReward.attraction.attractionName))
					.count() == 0) {
				userRewards.add(userReward);
			}
		} finally {
			lockUserRewards.unlock();
		}
		// }
	}

	/**
	 * @return List<UserReward>
	 */
	public List<UserReward> getUserRewards() {
		lockUserRewards.lock();
		try {
			return userRewards;
		} finally {
			lockUserRewards.unlock();
		}
	}

	/**
	 * @return UserPreferences
	 */
	public UserPreferences getUserPreferences() {
		return userPreferences;
	}

	/**
	 * @param userPreferences
	 */
	public void setUserPreferences(UserPreferences userPreferences) {
		this.userPreferences = userPreferences;
	}

	/**
	 * @return VisitedLocation
	 */
	public VisitedLocation getLastVisitedLocation() {
		return visitedLocations.get(visitedLocations.size() - 1);
	}

	/**
	 * @param tripDeals
	 */
	public void setTripDeals(List<Provider> tripDeals) {
		this.tripDeals = tripDeals;
	}

	/**
	 * @return List<Provider>
	 */
	public List<Provider> getTripDeals() {
		return tripDeals;
	}

}
