package tourGuide.model;

import gpsUtil.location.VisitedLocation;
import tourGuide.model.user.User;

public class UserLocation {
    private VisitedLocation visitedLocation;
    private User user;

    public UserLocation(VisitedLocation visitedLocation, User user) {
        this.visitedLocation = visitedLocation;
        this.user = user;
    }

    /**
     * @return VisitedLocation
     */
    public VisitedLocation getVisitedLocation() {
        return visitedLocation;
    }

    /**
     * @param visitedLocation
     */
    public void setVisitedLocation(VisitedLocation visitedLocation) {
        this.visitedLocation = visitedLocation;
    }

    /**
     * @return User
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return int
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((visitedLocation == null) ? 0 : visitedLocation.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    /**
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserLocation other = (UserLocation) obj;
        if (visitedLocation == null) {
            if (other.visitedLocation != null)
                return false;
        } else if (!visitedLocation.equals(other.visitedLocation))
            return false;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!user.equals(other.user))
            return false;
        return true;
    }

}
