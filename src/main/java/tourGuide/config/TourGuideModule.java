package tourGuide.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import gpsUtil.GpsUtil;
import rewardCentral.RewardCentral;

@Configuration
public class TourGuideModule {

	/**
	 * @return GpsUtil
	 */
	@Bean
	public GpsUtil getGpsUtil() {
		return new GpsUtil();
	}

	/**
	 * @return RewardCentral
	 */
	@Bean
	public RewardCentral getRewardCentral() {
		return new RewardCentral();
	}

}
