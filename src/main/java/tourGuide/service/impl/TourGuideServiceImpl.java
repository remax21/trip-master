package tourGuide.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.UserLocation;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.service.TourGuideService;
import tourGuide.tracker.Tracker;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideServiceImpl implements TourGuideService {
	private Logger logger = LoggerFactory.getLogger(TourGuideServiceImpl.class);
	private final GpsUtil gpsUtil;
	private final RewardsServiceImpl rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	boolean testMode = true;
	private ExecutorService service = Executors.newFixedThreadPool(100);

	public TourGuideServiceImpl(GpsUtil gpsUtil, RewardsServiceImpl rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;

		if (testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}

	/**
	 * @param user
	 * @return List<UserReward>
	 */
	@Override
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}

	/**
	 * @param user
	 * @return VisitedLocation
	 */
	@Override
	public VisitedLocation getUserLocation(User user) {
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ? user.getLastVisitedLocation()
				: trackUserLocation(user);
		return visitedLocation;
	}

	/**
	 * @param userName
	 * @return User
	 */
	@Override
	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}

	/**
	 * @return List<User>
	 */
	@Override
	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}

	/**
	 * @param user
	 */
	@Override
	public void addUser(User user) {
		if (!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}

	/**
	 * @param user
	 * @return List<Provider>
	 */
	@Override
	public List<Provider> getTripDeals(User user) {
		int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(),
				user.getUserPreferences().getNumberOfAdults(),
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(),
				cumulatativeRewardPoints);
		user.setTripDeals(providers);
		return providers;
	}

	/**
	 * @param user
	 * @return VisitedLocation
	 */
	@Override
	public VisitedLocation trackUserLocation(User user) {
		VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
		user.addToVisitedLocations(visitedLocation);
		rewardsService.calculateRewards(user);
		return visitedLocation;
	}

	/**
	 * @param users
	 * @return List<VisitedLocation>
	 */
	@Override
	public List<VisitedLocation> trackAllUsersLocations(List<User> users) {
		List<VisitedLocation> visitedLocations = new ArrayList<>();
		List<Callable<UserLocation>> visitedLocationCallables = users.stream()
				.map((user) -> getCallableVisitedLocation(user)).collect(Collectors.toList());
		try {
			List<Future<UserLocation>> futures = service.invokeAll(visitedLocationCallables);
			for (Future<UserLocation> future : futures) {
				UserLocation userLocation = future.get();
				User user = userLocation.getUser();
				VisitedLocation visitedLocation = userLocation.getVisitedLocation();
				logger.debug("END track user location of user {}", user.getUserName());
				visitedLocations.add(visitedLocation);
				user.addToVisitedLocations(visitedLocation);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return visitedLocations;

	}

	/**
	 * @param user
	 * @return Callable<UserLocation>
	 */
	private Callable<UserLocation> getCallableVisitedLocation(User user) {
		return () -> {
			logger.debug("START track user location of user {}", user.getUserName());
			return new UserLocation(gpsUtil.getUserLocation(user.getUserId()), user);
		};
	}

	/**
	 * @param visitedLocation
	 * @return List<Attraction>
	 */
	@Override
	public List<Attraction> getNearByAttractions(VisitedLocation visitedLocation) {
		Map<Double, Attraction> attractionsByDistances = new TreeMap<>();

		for (Attraction attraction : gpsUtil.getAttractions()) {
			attractionsByDistances.put(rewardsService.getDistance(attraction, visitedLocation.location),
					attraction);
		}

		return new ArrayList<>(attractionsByDistances.values()).subList(0, 5);
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				tracker.stopTracking();
			}
		});
	}

	public void shutdownService() {
		service.shutdown();
	}

	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes
	// internal users are provided and stored in memory
	private final Map<String, User> internalUserMap = new HashMap<>();

	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			internalUserMap.put(userName, user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}

	/**
	 * @param user
	 */
	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i -> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(),
					new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}

	/**
	 * @return double
	 */
	private double generateRandomLongitude() {
		double leftLimit = -180;
		double rightLimit = 180;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	/**
	 * @return double
	 */
	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
		double rightLimit = 85.05112878;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	/**
	 * @return Date
	 */
	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
		return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}

}
